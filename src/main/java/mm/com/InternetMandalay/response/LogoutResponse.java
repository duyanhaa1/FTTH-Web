package mm.com.InternetMandalay.response;

import lombok.Data;

@Data
public class LogoutResponse {
    private String message;
}
