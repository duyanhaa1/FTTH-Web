package mm.com.InternetMandalay.exception;

public class ErrorCode {
    public static String NOT_FOUND_EXCEPTION = "404";
    public static String BAD_REQUEST = "400";
    public static String LOGIN_EXCEPTION = "401";
}
