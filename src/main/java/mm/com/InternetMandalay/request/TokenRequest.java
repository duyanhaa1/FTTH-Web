package mm.com.InternetMandalay.request;

import lombok.Data;

@Data
public class TokenRequest {
    private String refreshToken;
}
