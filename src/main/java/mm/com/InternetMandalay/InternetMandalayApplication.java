package mm.com.InternetMandalay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InternetMandalayApplication {

	public static void main(String[] args) {
		SpringApplication.run(InternetMandalayApplication.class, args);
	}

}
